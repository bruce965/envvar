﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace EnvVar
{
	public static class Program
	{
		const char ESCAPE_CHAR = '"';

		public enum Action {
			Exist, Get, Set, Unset, Add, Remove
		}

		public static readonly string RepositoryURL = "https://gitlab.com/bruce965/envvar";
		static readonly string licenseShort = "Released under The MIT License.";

		public static void Main(string[] args)
		{
			var modifiers = args.TakeWhile(arg => arg.StartsWith("/") && arg.Length == 2);
			var otherArgs = args.Skip(modifiers.Count());

			if (args.Length == 0)
			{
				dieHelp();
				return;
			}

			if (otherArgs.Count() > 3)
			{
				dieTooManyArguments();
				return;
			}

			if (otherArgs.Count() < 2)
			{
				dieRequiredArgumentMissing();
				return;
			}

			bool caseSensitive = false;
			bool systemWide = false;
			foreach (var modifier in modifiers) {
				switch (modifier[1]) {
					case 'C':
						caseSensitive = true;
						break;

					case 'M':
						systemWide = true;
						break;

					default:
						dieInvalidArgument("modifier", modifier);
						return;
				}
			}

			Action action;
			switch (otherArgs.ElementAt(0).ToUpperInvariant()) {
				case "EXIST":
					action = Action.Exist;
					break;

				case "GET":
					action = Action.Get;
					break;

				case "SET":
					action = Action.Set;
					break;

				case "UNSET":
					action = Action.Unset;
					break;

				case "ADD":
					action = Action.Add;
					break;

				case "REMOVE":
					action = Action.Remove;
					break;

				default:
					dieInvalidArgument("action", otherArgs.ElementAt(0));
					return;
			}

			var variable = otherArgs.ElementAt(1);
			var value = (otherArgs.Count() >= 3) ? otherArgs.ElementAt(2) : null;
			Run(action, variable, value, caseSensitive, systemWide);
			Environment.Exit(0);
		}

		public static void Run(Action action, string variable, string value, bool caseSensitive, bool systemWide)
		{
			var target = systemWide ? EnvironmentVariableTarget.Machine : EnvironmentVariableTarget.User;

			switch (action)
			{
				case Action.Exist:
					Environment.Exit(Environment.GetEnvironmentVariable(variable, target) == null ? 1 : 0);
					break;

				case Action.Get:
					Console.Write(Environment.GetEnvironmentVariable(variable, target));
					break;
				
				case Action.Set:
					Environment.SetEnvironmentVariable(variable, value, target);
					break;

				case Action.Unset:
					Environment.SetEnvironmentVariable(variable, null, target);
					break;

				case Action.Add:
				case Action.Remove:
					var values = splitMultiVariable(Environment.GetEnvironmentVariable(variable, target))
						.Where(value2 => !value2.Equals(value, caseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase));

					if (action == Action.Add)
						values = values.Concat(new[] { value });

					Environment.SetEnvironmentVariable(variable, unsplitMultiVariable(values), target);
					break;
			}
		}

		static IEnumerable<string> splitMultiVariable(string value)
		{
			var escapeString = new String(ESCAPE_CHAR, 1);

			// we start with something like:
			// C:\somedir;;c:\somethingelse\;"C:\path;with;semicolons";"ab;c"def;";a;b;c;";%USERPROFILE%\AppData\banana;""

			// we should end up with a sequence like:
			// C:\somedir
			// c:\somethingelse\
			// "C:\path;with;semicolons"
			// "ab;c"def
			// ";a;b;c;"
			// %USERPROFILE%\AppData\banana
			// ""

			// NOTE: '/', '"', '<', '>' and '|' chars are normally not accepted in multi-value variables.
			// Still, we must handle the `"ab;c"def` case somehow. We could have done otherwise, but
			// splitting it this way will permit unsplitting it exactly as it was before.

			if (value == null)
				yield break;

			var isEscaped = false;
			var escapedChunks = new List<string>();
			foreach (var chunk in value.Split(Path.PathSeparator))
			{
				if (escapedChunks.Count == 0)
				{
					// this is a new chunk...
					if (chunk.StartsWith(escapeString))
						isEscaped = true;  // ...and it starts with the escape character
				}
				else
				{
					// this is not a new chunk...
					if (chunk.Contains(escapeString))
						isEscaped = false;  // ...and it contains the escape character
				}

				escapedChunks.Add(chunk);
				if (!isEscaped)
				{
					yield return String.Join(Path.PathSeparator.ToString(), escapedChunks.ToArray());
					escapedChunks.Clear();
				}
			}

			if (escapedChunks.Count > 0)
				yield return String.Join(Path.PathSeparator.ToString(), escapedChunks.ToArray());
		}

		static string unsplitMultiVariable(IEnumerable<string> values)
		{
			return String.Join(Path.PathSeparator.ToString(), values.ToArray());
		}

		static void dieInvalidArgument(string name, string option)
		{
			Console.Error.WriteLine("ERROR: invalid {0} \"{1}\".", name, option);
			Environment.Exit(-1);
		}

		static void dieTooManyArguments()
		{
			Console.Error.WriteLine("ERROR: too many arguments.");
			Environment.Exit(-1);
		}

		static void dieRequiredArgumentMissing()
		{
			Console.Error.WriteLine("ERROR: required argument missing.");
			Environment.Exit(-1);
		}

		static void dieHelp()
		{
			var message = @"
{1}
{2}
{3}

{4}

USAGE

  {0} [/C] [/M] action variable [value]

OPTIONS

  /C        Check for case-sensitive match with REMOVE action.

  /M        Set in the system-wide environment.

  action    What action to perform on the environment variable.
    EXIST     Check for a persistent variable, status code 0 if exists.
    GET       Get the value of a persistent variable.
    SET       Set a variable to 'value'.
    UNSET     Unset a variable.
    ADD       Add 'value' to a multi-value variable.
    REMOVE    Remove 'value' from a multi-value variable.

  variable  The variable to be modified.

  value     The value.

EXAMPLES

  {0} SET ""JAVA_HOME"" ""C:\Java\bin""
    Permanently set JAVA_HOME environment variable for current user.

  {0} /M UNSET ""JAVA_HOME""
    Permanently unset JAVA_HOME environment variable for every user.

  {0} /M ADD ""PATH"" ""%CD%""
    Permanently add current working directory to PATH for every user.

  {0} REMOVE ""PATH"" ""%CD%""
    Permanently remove current working directory from PATH for current user.

  {0} /C REMOVE ""MY_MULTIVAR"" ""ABC""
    Permanently remove ABC value from multi-value variable MY_MULTIVAR for current user, ensuring case matches.

  {0} /M EXIST ""JAVA_HOME""
  IF %ERRORLEVEL% NEQ 0 {0} /M SET ""JAVA_HOME"" ""C:\Java\bin""
    Check if the the JAVA_HOME is set system-wide and if not, set it.

  {0} /M GET ""ENV_VAR""
    Print ENV_VAR system-wide environment variable.
";

			Console.Write(
				String.Join(Environment.NewLine, message.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None).Skip(1).ToArray()),
				Path.GetFileNameWithoutExtension(AppDomain.CurrentDomain.FriendlyName),
				typeof(Program).Assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), true).OfType<AssemblyCopyrightAttribute>().FirstOrDefault()?.Copyright,
				licenseShort,
				RepositoryURL,
				typeof(Program).Assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), true).OfType<AssemblyDescriptionAttribute>().FirstOrDefault()?.Description
			);

			Environment.Exit(5);
		}
	}
}
