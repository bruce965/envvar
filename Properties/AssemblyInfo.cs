﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("EnvVar")]
[assembly: AssemblyDescription("Modify user or system environment variables.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("EnvVar")]
[assembly: AssemblyCopyright("Copyright (c) 2017 Fabio Iotti")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("33c0ceec-1949-4e24-bcfa-10a15c1f886c")]

[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.1")]